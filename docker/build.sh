#!/bin/bash

git clone git@bitbucket.org:datatoknowledge/webpagetraverserwrapper.git

git clone git@bitbucket.org:fabiofumarola/listflattener.git

git clone git@bitbucket.org:fabianaLanotte/datarecordaligner.git

docker build -t kdde/datarecordaligner .

rm -rf webpagetraverserwrapper
rm -rf listflattener
rm -rf datarecordaligner

echo "---------------------------------------"
echo "please run the docker with the command "
echo "docker run -dt --name datarecordaligner -p 4569:4569 kdde/datarecordaligner"

