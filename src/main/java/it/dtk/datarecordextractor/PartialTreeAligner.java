package it.dtk.datarecordextractor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import it.dtk.datarecordextractor.model.TreeAlignment;
import it.dtk.datarecordextractor.model.WebElementComparator;
import it.dtk.datarecordextractor.treematcher.SimpleTreeMatching;
import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebPage;
import it.dtk.datarecordextractor.util.*;


/**
 *
 * @author Colucci Pasquale
 *
 */
public class PartialTreeAligner {

	public WebElement seed;
	//Mappa risultante da cui ricavare json, tabella html, etc
	private Map<WebElement, String[]> mapResult = new HashMap<WebElement, String[]>();
	//Lista dei nodi che non matchano e non inseribili nell'albero seme
	private Map<WebElement, String[]> nodeNotIncluded = new HashMap<WebElement, String[]>(); ;
	//Memorizza la posizione originale dei webelemnt.
	private Map<Integer, Integer> mapPosDataRecord;
	//lista ordinata delle nodi che saranno inclusi nel json
	private List<String[]> listaOrder = new ArrayList<String[]>();
	//mappa contenete gli attributi "alt" dei tagnode img
	private Map<WebElement, String[]> mapResultAttributeAlt = new HashMap<WebElement, String[]>();
	//mappa contenete gli attributi "alt" dei tagnode img che non matchano
	private Map<WebElement, String[]> mapNodeNotIncludedAlt = new HashMap<WebElement, String[]>();

	//lista di alberi che non matchano a partire dalla radice
	private List<WebElement> listTreeNotAligned = new ArrayList<WebElement>();

	/**
	 * Method that makes the multiple tree aligmnet based on partial
	 * alignment of two tag tree.
	 *
	 * @param dataRecords: set of webelements
	 */
	public void alignDataRecords(List<WebElement> dataRecords) {

		mapPosDataRecord = new HashMap<Integer, Integer>();
		for (int i = 0; i < dataRecords.size(); i++) {
			mapPosDataRecord.put(dataRecords.get(i).getId(), i);
		}

		Collections.sort(dataRecords, new WebElementComparator());
		WebElement record0 = dataRecords.get(0);
		seed = clone(record0);
		List<WebElement> treesNotAligned = new ArrayList<WebElement>();
		int seedNodes = seed.getNumNodes();

		while (!dataRecords.isEmpty()) {

			WebElement currentTree = dataRecords.remove(0);
			TreeAlignment treeAligned = SimpleTreeMatching.align(seed, currentTree);

			//se ci sono nodi che non matchano si cerca di inserirli nel seed
			if (treeAligned.getScore() < currentTree.getNumNodes()) {
				boolean inserted = generateSeed(currentTree, treeAligned, seed, mapPosDataRecord.get(currentTree.getId()));
				if (!inserted) {
					treesNotAligned.add(currentTree);
				}
			}
			//Se treeAligned contiene un punteggio == 0,allora due alberi hanno radice diversa e non allineabili
			if(treeAligned.getScore() != 0 ){
				setResult(treeAligned, mapPosDataRecord.get(currentTree.getId()));
			} 
			if (dataRecords.isEmpty()) {
				//se sono stati inseriti dei nodi in seed, ripeti l'elaborazione
				if (seedNodes != seed.getNumNodes()) {

					dataRecords.addAll(0, treesNotAligned);
					treesNotAligned = new ArrayList<WebElement>();
					seedNodes = seed.getNumNodes();
				}
			}
		}
		//Nel caso ci fossero alberi che a partire dalla radice non matchano
		//i nodi di ogni singolo albero da inserire nella tabella finale o nel json 
		//vengono inseriti nella lista finale come singole colonne.
		listTreeNotAligned = treesNotAligned;
		for(WebElement node : treesNotAligned){
			insertTreeNotAligned(node);
		}
	}

	/**
	 *  Inserisce in mapResult tutti e soli i valori degli innerText, del datarecord avente posizione "dataRecordPos", diversi da null.
	 *
	 * @param tree alignment tree between datarecord having "dataRecordPos" position and seed
	 * @param dataRecordPos original position of datarecord
	 */
	private void setResult(TreeAlignment tree, int dataRecordPos) {
		//verifia se il il nodo è di tipo img
		if(tree.getSecondNode().getNodeTag().equals("img")){
			String[] list = mapResult.getOrDefault(tree.getFirstNode(), new String[mapPosDataRecord.size()]);
			String[] listAttributeAlt = mapResultAttributeAlt.getOrDefault(tree.getFirstNode(), new String[mapPosDataRecord.size()]);
			if((list[dataRecordPos]== null) && (listAttributeAlt[dataRecordPos]== null)){ 
				if( (tree.getSecondNode().getAttributes().containsKey("alt")) && (tree.getSecondNode().getAttributes().get("alt")!= "") && (tree.getSecondNode().getAttributes().get("alt")!= null) ){
					listAttributeAlt[dataRecordPos] = "alt= "+tree.getSecondNode().getAttributes().get("alt");
					mapResultAttributeAlt.put(tree.getSecondNode(), listAttributeAlt);
				}
				if( (tree.getSecondNode().getAttributes().containsKey("original")) &&(tree.getSecondNode().getAttributes().get("original")!= "") && (tree.getSecondNode().getAttributes().get("original")!= null) ){
					list[dataRecordPos] = " src= "+tree.getSecondNode().getAttributes().get("original");
				} else {
					list[dataRecordPos] = " src= "+tree.getSecondNode().getAttributes().get("src");
				}
				mapResult.put(tree.getFirstNode(), list);
			}
		}
		//verifia se il il nodo è di tipo a
		if(tree.getSecondNode().getNodeTag().equals("a") && tree.getSecondNode().getAttributes().containsKey("href")){

			if(tree.getSecondNode().getAttributes().get("href").startsWith("http")){
				String[] list = mapResult.getOrDefault(tree.getFirstNode(), new String[mapPosDataRecord.size()]);
				if(list[dataRecordPos] == null){
					list[dataRecordPos] = tree.getSecondNode().getAttributes().get("href");
					mapResult.put(tree.getFirstNode(), list);
				}	
			//caso in cui il tag <a> non è un link esterno ma contine del testo
			} else if(tree.getSecondNode().getText()!= ""){
				String[] list = mapResult.getOrDefault(tree.getFirstNode(), new String[mapPosDataRecord.size()]);
				if(list[dataRecordPos] == null){
					list[dataRecordPos] = tree.getSecondNode().getText();
					mapResult.put(tree.getFirstNode(), list);
				}
			}
		} else {
			if(tree.getSecondNode().getText()!= "" ){ 
				String[] list = mapResult.getOrDefault(tree.getFirstNode(), new String[mapPosDataRecord.size()]);
				if(list[dataRecordPos] == null){
					list[dataRecordPos] = tree.getSecondNode().getText();
					mapResult.put(tree.getFirstNode(), list);
				}
			}
		}
		//inserisce gli innerText di tutti i nodi figli che matchano
		for(TreeAlignment t : tree.getSubTreeAlignment()){

			setResult(t, dataRecordPos);

			if(nodeNotIncluded.containsKey(t.getSecondNode())){
				nodeNotIncluded.remove(t.getSecondNode());
				if(mapNodeNotIncludedAlt.containsKey(t.getSecondNode())){
					mapNodeNotIncludedAlt.remove(t.getSecondNode());
				}
			}
		}
	}


	/**
	 * Method that verifies if all nodes have been inserted into seed tree
	 * @param currentTree:  current node
	 * @param treeAlignment treeAlignment between currentTree and seed             
	 * @param seed:         seed node
	 * @return true if all nodes have been inserted into seed, otherwise false
	 */
	private boolean generateSeed(WebElement currentTree, TreeAlignment treeAlignment, WebElement seed, int currentTreePosition) {

		if (!currentTree.getNodeTag().equals(seed.getNodeTag())) {
			return false;
		}

		//Map<K,V> dove K figlio di currentTree, V webElement figlio di seed che matcha con K
		Map<WebElement, WebElement> webElementsMap = new HashMap<WebElement, WebElement>();
		boolean res = true;
		ArrayList<WebElement> unmatchedNodes = new ArrayList<WebElement>();
		WebElement lastMatchedNode = null;

		for (TreeAlignment el : treeAlignment.getSubTreeAlignment()) 
			webElementsMap.put(el.getSecondNode(), el.getFirstNode());

		//cerca di inserire tutti i nodi che non matchano
		for (WebElement currentNode : currentTree.getChildren()) {

			//se currentNode matcha verifica prima se ci sono nodi che nonMatchano in unmatchedNodes e poi richiama generateseed su tutti i figli in treeAlignment
			if (webElementsMap.containsKey(currentNode)) {
				if (unmatchedNodes.isEmpty()) {
					lastMatchedNode = currentNode;
				} else {

					//verifica se currentNode è inseribile come primo figlio(sinistra) di "seed"
					if (lastMatchedNode == null) {

						boolean inserted = checkAddingLeft(currentNode, seed, currentTreePosition, unmatchedNodes,webElementsMap );
						if(inserted){
							unmatchedNodes= new ArrayList<>();
						}else{
							res=false;
							lastMatchedNode = currentNode;
							unmatchedNodes = new ArrayList<WebElement>();
						}
					} else {
						//verifica se currentNode è inseribile tra 2 figli di seed "seed"
						boolean inserted = checkAddingBetween(currentNode, lastMatchedNode, seed ,currentTreePosition, unmatchedNodes, webElementsMap);
						if(inserted){
							lastMatchedNode = currentNode;
							unmatchedNodes = new ArrayList<WebElement>();
						}else {
							res=false;
							lastMatchedNode = currentNode;
							unmatchedNodes = new ArrayList<WebElement>();
						}
					}
				}

				//richiama generateseed su tutti i figli in treeAlignment
				TreeAlignment subtreeAlignment = null;
				if(currentNode.getChildren().size()!=0 ) {
					for (TreeAlignment t : treeAlignment.getSubTreeAlignment()) {

						if (t.getSecondNode() == currentNode) {
							subtreeAlignment = t;
						}
					}
					if (subtreeAlignment.getSecondNode().getChildren().size() != 0) {
						res = generateSeed(currentNode, subtreeAlignment, webElementsMap.get(currentNode), currentTreePosition);
					}
				}
			} else {
				unmatchedNodes.add(currentNode);
			}
		}     
		//verifica se currentNode è inseribile come ultimo figlio di seed
		if (!unmatchedNodes.isEmpty()) {
			if (lastMatchedNode != null) {
				boolean inserted = checkAddingRight(lastMatchedNode, seed, currentTreePosition, unmatchedNodes, webElementsMap );
				if(!inserted){
					res = false;
				}
			} else {

				for(WebElement node : unmatchedNodes){
					addUnmatchedList(node, currentTreePosition);
				}

				res = false;
			}
		}
		return res;
	}

	private boolean checkAddingRight(WebElement lastMatchedNode, WebElement seed, int currentTreePosition, ArrayList<WebElement> unmatchedNodes, Map<WebElement, WebElement> webElementsMap) {
		//lastMatched node deve essere l'ultimo nodo in seedTree
		if (seed.getChildren().get(seed.getChildren().size() - 1).getNodeTag().equals(lastMatchedNode.getNodeTag())) {
			addChildrenRight(unmatchedNodes, seed, currentTreePosition);

			return true;
		} else {

			for(WebElement node : unmatchedNodes){
				addUnmatchedList(node, currentTreePosition);
				//addUnmatchedList2(node, currentTreePosition);
			}
			return false;
		}
	}

	private boolean checkAddingLeft(WebElement currentNode,WebElement seedNode , int currentNodePos ,ArrayList<WebElement> unmatchedNodes, Map<WebElement, WebElement> webElementsMap) {

		WebElement nodeseed2 = webElementsMap.get(currentNode);
		boolean res = true;

		if (seedNode.getChildren().get(0) != nodeseed2) {
			res = false;
			for(WebElement node : unmatchedNodes)
				addUnmatchedList(node, currentNodePos);
			//addUnmatchedList2(node, currentNodePos);

		} else {
			addChildrenLeft(unmatchedNodes, seedNode, currentNodePos);
		}
		return res;
	}

	private boolean checkAddingBetween(WebElement currentNode, WebElement lastMatchedNode, WebElement seedNode,int currentNodePos ,ArrayList<WebElement> unmatchedNodes, Map<WebElement, WebElement> webElementsMap) {

		WebElement nodeSeed1 = webElementsMap.get(lastMatchedNode);
		WebElement nodeSeed2 = webElementsMap.get(currentNode);

		for (int i = 0; i < seedNode.getChildren().size(); i++) {

			if (seedNode.getChildren().get(i) == nodeSeed1) {

				if (seedNode.getChildren().get(i + 1) == nodeSeed2) {
					addChildrenBetween(unmatchedNodes, seedNode, nodeSeed1, currentNodePos);
					return true;
				} else {

					for(WebElement node : unmatchedNodes){
						addUnmatchedList(node, currentNodePos);
						//addUnmatchedList2(node, currentNodePos);
					}
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * method to insert nodes in the tree seed to the left
	 *
	 * @param unMatch
	 * @param parent
	 */
	private void addChildrenLeft(List<WebElement> unMatch, WebElement parent, int dataRecordPos) {
		List<WebElement> children = parent.getChildren();
		children.addAll(0, unMatch);
		seed.setNumNodes(seed.getNumNodes() + unMatch.size());
		for(WebElement node : unMatch){
			setMapResult(node, dataRecordPos);
		}
	}
	/**
	 * method to insert nodes in the tree seed to the right
	 *
	 * @param unMatch
	 * @param parent
	 */
	private void addChildrenRight(List<WebElement> unMatch, WebElement parent, int dataRecordPos) {
		List<WebElement> children = parent.getChildren();
		children.addAll(children.size(), unMatch);
		seed.setNumNodes(seed.getNumNodes() + unMatch.size());

		for(WebElement node : unMatch){
			setMapResult(node, dataRecordPos);

		}
	}

	/**
	 * method to insert nodes in the tree seed, case 1
	 *
	 * @param unMatch
	 * @param parent
	 * @param nodeMatch
	 */
	private void addChildrenBetween(List<WebElement> unMatch, WebElement parent, WebElement nodeMatch, int dataRecordPos) {
		List<WebElement> children = parent.getChildren();
		for (int i = 0; i < children.size(); i++) {
			if (children.get(i) == nodeMatch) {
				int pos = i + 1;
				children.addAll(pos, unMatch);
				break;
			}
		}
		seed.setNumNodes(seed.getNumNodes() + unMatch.size());

		for(WebElement node : unMatch){
			setMapResult(node, dataRecordPos);

		}
	}

	/**
	 *
	 * @param node
	 * @param id
	 */
	private void setMapResult(WebElement node, int dataRecordPos){
		//verifia se il il nodo è di tipo img
		if(node.getNodeTag().equals("img")){
			String[] list = new String[mapPosDataRecord.size()];
			if( (node.getAttributes().containsKey("alt")) && (node.getAttributes().get("alt")!= null)&&(node.getAttributes().get("alt")!= "") ){
				String[] listAttributeAlt = new String[mapPosDataRecord.size()];
				listAttributeAlt[dataRecordPos] = "alt= "+ node.getAttributes().get("alt");
				mapResultAttributeAlt.put(node, listAttributeAlt);
			} 
			if( (node.getAttributes().containsKey("original"))&&(node.getAttributes().get("original")!= "") && (node.getAttributes().get("original")!= null) ){
				list[dataRecordPos] = "src= "+node.getAttributes().get("original");
			} else {
				list[dataRecordPos] = "src= "+node.getAttributes().get("src");
			}
			mapResult.put(node, list);
		}
		//verifia se il il nodo è di tipo a
		if(node.getNodeTag().equals("a") && node.getAttributes().containsKey("href")){
			if(node.getAttributes().get("href").startsWith("http")){
				String[] list = new String[mapPosDataRecord.size()];
				list[dataRecordPos] = node.getAttributes().get("href");
				mapResult.put(node, list);
				//caso in cui <a> non è in link esterno ma contiene del testo
			} else if(node.getText()!= ""){
				String[] list = new String[mapPosDataRecord.size()];
				list[dataRecordPos] = node.getText();
				mapResult.put(node, list);
			}
		//caso in cui il nodo non è <img> e <a>
		} else {		
			if(node.getText()!= ""){
				String[] list = new String[mapPosDataRecord.size()];
				list[dataRecordPos] = node.getText();
				mapResult.put(node, list);
			}
		}
		for(WebElement child: node.getChildren()){
			setMapResult(child, dataRecordPos);
		}
	}

	/**
	 * Insert node, belong to datarecord with position dataRecordPos, and its children in nodeNotIncluded
	 * @param node
	 * @param dataRecordPos
	 */
	private void addUnmatchedList(WebElement node, int dataRecordPos){
		//verifia se il il nodo è di tipo img
		if(node.getNodeTag().equals("img")){
			String[] list = new String[mapPosDataRecord.size()];
			if( (node.getAttributes().containsKey("alt")) && (node.getAttributes().get("alt")!= "") && (node.getAttributes().get("alt")!= null) ){
				String[] listAttributeAlt = new String[mapPosDataRecord.size()];
				listAttributeAlt[dataRecordPos] = "alt= "+ node.getAttributes().get("alt");
				mapNodeNotIncludedAlt.put(node, listAttributeAlt);
			} 
			if( (node.getAttributes().containsKey("original"))&&(node.getAttributes().get("original")!= "") && (node.getAttributes().get("original")!= null) ){
				list[dataRecordPos] = "src= "+node.getAttributes().get("original");
			} else { 
				list[dataRecordPos] = "src= "+node.getAttributes().get("src");
			}
			nodeNotIncluded.put(node, list);
		}
		//verifia se il il nodo è di tipo a
		if(node.getNodeTag().equals("a") && node.getAttributes().containsKey("href")){
			if(node.getAttributes().get("href").startsWith("http")){
				String[] list = new String[mapPosDataRecord.size()];
				list[dataRecordPos] = node.getAttributes().get("href");
				nodeNotIncluded.put(node, list);
			} else if(node.getText()!= ""){
				String[] list = new String[mapPosDataRecord.size()];
				list[dataRecordPos] = node.getText();
				nodeNotIncluded.put(node, list);
			}
		} else {		
			//FIXME non sarebbe più opportuno mantenere solo webelement e id e solo alla fine generare l'array di stringhe?
			if(node.getText() != ""){

				String[] array = new String[mapPosDataRecord.size()];
				array[dataRecordPos] = node.getText();
				nodeNotIncluded.put(node, array);
			}
		}
		for(WebElement child : node.getChildren()){
			addUnmatchedList(child, dataRecordPos);
		}

	}


	/**
	 * Service method, creates a new webelemnt, by assigning the values of
	 * the original tree
	 *
	 * @param original: original webelemnt
	 * @return new webelement
	 */
	private WebElement createWebElement(WebElement original) {
		WebElement newNode = new WebElement();
		newNode.setId((original.getId()));
		newNode.setNodeTag(original.getNodeTag());
		newNode.setNumNodes(original.getNumNodes());
		newNode.setAttributes((Map<String, String>) ((HashMap<String, String>) original.getAttributes()).clone());
		newNode.setParentPath(original.getParentPath());
		newNode.setParentCSSPath(original.getParentCSSPath());
		newNode.setParentDomCSSPath(original.getParentDomCSSPath());
		newNode.setPosition(original.getPosition());
		newNode.setSize(original.getSize());
		newNode.setText(original.getText());
		newNode.setPosition(original.getPosition());
		newNode.setWebPage(original.getWebPage());
		newNode.setWebList(original.getWebList());
		ArrayList<WebElement> list = new ArrayList<WebElement>();
		for (int i = 0; i < original.getChildren().size(); i++) {
			list.add(clone(original.getChildren().get(i)));
		}
		newNode.setChildren(list);
		return newNode;
	}

	/**
	 * Method to clone a tree
	 *
	 * @param nodeOriginal: original webelemnt
	 * @return webelemnt cloned
	 */
	private WebElement clone(WebElement nodeOriginal) {
		WebElement node = createWebElement(nodeOriginal);
		for (WebElement element : nodeOriginal.getChildren()) {
			clone(element);
		}
		return node;
	}

	/**
	 * Method to insert in an orderly the nodes that represent the columns in a list
	 * 
	 */
	private void insertMatchNodeIntoListOrder(WebElement node){
		if(mapResult.containsKey(node)){
			if(node.getNodeTag().equals("img")){
				listaOrder.add(mapResult.get(node));
				if(mapResultAttributeAlt.containsKey(node)){
					listaOrder.add(mapResultAttributeAlt.get(node));
				}
			} else {
				listaOrder.add(mapResult.get(node));
			}
		} 
		for(WebElement n : node.getChildren()){
			insertMatchNodeIntoListOrder(n);
		}
	}

	/**
	 * Method to insert in an orderly the nodes that represent the columns in a list
	 */
	private void insertNodeUnMatchIntoListOrder(){
		for(Entry<WebElement, String[]> set : nodeNotIncluded.entrySet()){
			if(set.getKey().equals("img")){
				if(mapNodeNotIncludedAlt.containsKey(set.getKey())){
					listaOrder.add(set.getValue());
				}
			}
			listaOrder.add(set.getValue());
		}
	}

	/**
	 * Create to json
	 * @return string of json
	 */
	public String stringJson(){
		String stringJson="";
		insertMatchNodeIntoListOrder(seed);
		insertNodeUnMatchIntoListOrder();
		Column[] coll = new Column[listaOrder.size()];
		int i = 0;
		int j= 0;
		for(String[] set : listaOrder){
			//Row[] riga = new Row[set.length];
			List<Row> riga = new ArrayList<Row>();
			for(i=0; i < set.length; i++){
				if(set[i] != null){
					//riga[i] = new Row(i, set[i]);
					riga.add(new Row(i, set[i]));
				}
			}
			coll[j] = new Column(j, riga);
			j++;
		}
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
			stringJson= mapper.writeValueAsString(coll);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stringJson;
	}

	/**
	 *
	 * @return seed
	 */
	public WebElement getSeed() {
		return seed;
	}

	public void setSeed(WebElement seed) {
		this.seed = seed;
	}

	public Map<WebElement, String[]> getMapResult() {
		return mapResult;
	}

	public void setMapResult(Map<WebElement, String[]> mapResult) {
		this.mapResult = mapResult;
	}

	public Map<WebElement, String[]> getNodeNotIncluded() {
		return nodeNotIncluded;
	}

	public void setNodeNotIncluded(Map<WebElement, String[]> nodeNotIncluded) {
		this.nodeNotIncluded = nodeNotIncluded;
	}

	public Map<Integer, Integer> getMapPosDataRecord() {
		return mapPosDataRecord;
	}

	public void setMapPosDataRecord(Map<Integer, Integer> mapPosDataRecord) {
		this.mapPosDataRecord = mapPosDataRecord;
	}

	public List<String[]> getListaOrder() {
		return listaOrder;
	}

	public void setListaOrder(List<String[]> listaOrder) {
		this.listaOrder = listaOrder;
	}


	//Chiedere se va bene: Caso in cui a partire dalla radice due alberi non matchano.
	private boolean insertTreeNotAligned(WebElement node){
		final int DIMENSIONE = 1;
		boolean insert = false;
		//verifia se il il nodo è di tipo img
		if(node.getNodeTag().equals("img")){
			String[] list = new String[DIMENSIONE];
			if( (node.getAttributes().containsKey("alt")) && (node.getAttributes().get("alt")!= "") && (node.getAttributes().get("alt")!= null) ){
				String[] listAttributeAlt = new String[mapPosDataRecord.size()];
				listAttributeAlt[0] = "alt= "+ node.getAttributes().get("alt");
				mapNodeNotIncludedAlt.put(node, listAttributeAlt);
			} 
			if( (node.getAttributes().containsKey("original"))&&(node.getAttributes().get("original")!= "") && (node.getAttributes().get("original")!= null) ){
				list[0] = "src= "+node.getAttributes().get("original");
			} else { 
				list[0] = "src= "+node.getAttributes().get("src");
			}
			nodeNotIncluded.put(node, list);
		}
		//verifia se il il nodo è di tipo a
		if(node.getNodeTag().equals("a") && node.getAttributes().containsKey("href")){
			if(node.getAttributes().get("href").startsWith("http")){
				String[] list = new String[DIMENSIONE];
				list[0] = node.getAttributes().get("href");
				nodeNotIncluded.put(node, list);
				//caso in cui il tag <a> non sia un link esterno ma contiene del testo da estrarre.
			} else if(node.getText()!= ""){
				String[] list = new String[mapPosDataRecord.size()];
				list[0] = node.getText();
				nodeNotIncluded.put(node, list);
			}
		} else {		
			if(node.getText() != ""){
				String[] array = new String[DIMENSIONE];
				array[0] = node.getText();
				nodeNotIncluded.put(node, array);
			}
		}
		for(WebElement child : node.getChildren()){
			insertTreeNotAligned(child);
		}
		return insert;
	}

}
