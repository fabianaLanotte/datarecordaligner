package it.dtk.datarecordextractor.exception;

public class RootUnAlignedException extends RuntimeException {

	public RootUnAlignedException(){
		super("Root non allineata con l'albero seed");
	}
}
