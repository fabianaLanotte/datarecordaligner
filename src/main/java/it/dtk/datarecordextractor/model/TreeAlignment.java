package it.dtk.datarecordextractor.model;

import it.dtk.listflattener.model.WebElement;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TreeAlignment{
	
	private double score;
	private WebElement firstNode;
	private WebElement secondNode;
	private LinkedList<TreeAlignment> subTreeAlignment = new LinkedList<TreeAlignment>();

    public TreeAlignment(){}
    
	public TreeAlignment(WebElement firstNode, WebElement secondNode){
		setFirstNode( firstNode );
		setSecondNode( secondNode );
	}

	public void setScore(double score){
		this.score = score;
	}

	public void setFirstNode(WebElement firstNode){
		this.firstNode = firstNode;
	}

	public void setSecondNode(WebElement secondNode){
		this.secondNode = secondNode;
	}

	public void add(TreeAlignment alignment){
		subTreeAlignment.push(alignment);
	}

	public void addSubTreeAlignment(List<TreeAlignment> listAlignment){
		subTreeAlignment.addAll( listAlignment );
	}

	public double getScore(){
		return score;
	}

	public WebElement getFirstNode(){
		return firstNode;
	}

	public WebElement getSecondNode(){
		return secondNode;
	}

	public List<TreeAlignment> getSubTreeAlignment(){
		return subTreeAlignment;
	}
}