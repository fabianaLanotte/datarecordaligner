package it.dtk.datarecordextractor.model;

import it.dtk.listflattener.model.WebElement;

import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

/**
 * Compare two web elements using the number of their descents
 * @author Colucci Pasquale
 *
 */
public class WebElementComparator implements Comparator<WebElement> {
	//Metodo originale: basato solo sul numero di nodi
	/*
	public int compare(WebElement arg0, WebElement arg1) {

		if(arg0.getNumNodes()>arg1.getNumNodes()){
			return Integer.MIN_VALUE;
		}else if(arg0.getNumNodes()<arg1.getNumNodes()){
			return Integer.MAX_VALUE;
		}
		return 0;
	}
	*/
	
	/**
	 * altrenativa basata sull'uguaglianza del tagnode e sul numero dei nodi.
	 * se un albero ha una radice diversa da gli atri lo inserisce in coda.
	 *
	 */
	public int compare(WebElement arg0, WebElement arg1){
		if(arg0.getNodeTag().equals(arg1.getNodeTag())){
			if(arg0.getNumNodes()>arg1.getNumNodes()){
				return Integer.MIN_VALUE;
			}else if(arg0.getNumNodes()<arg1.getNumNodes()){
				return Integer.MAX_VALUE;
			}
			return 0;
		} 
		return Integer.MIN_VALUE;
	}
	
	public static <T, U extends Comparable<? super U>> Comparator<T> comparing(
			Function<? super T, ? extends U> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public static <T, U> Comparator<T> comparing(
			Function<? super T, ? extends U> arg0, Comparator<? super U> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public static <T> Comparator<T> comparingDouble(
			ToDoubleFunction<? super T> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public static <T> Comparator<T> comparingInt(ToIntFunction<? super T> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public static <T> Comparator<T> comparingLong(ToLongFunction<? super T> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public static <T extends Comparable<? super T>> Comparator<T> naturalOrder() {
		// TODO Auto-generated method stub
		return null;
	}

	public static <T> Comparator<T> nullsFirst(Comparator<? super T> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public static <T> Comparator<T> nullsLast(Comparator<? super T> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public static <T extends Comparable<? super T>> Comparator<T> reverseOrder() {
		// TODO Auto-generated method stub
		return null;
	}

	public Comparator<WebElement> reversed() {
		// TODO Auto-generated method stub
		return null;
	}

	public Comparator<WebElement> thenComparing(
			Comparator<? super WebElement> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public <U extends Comparable<? super U>> Comparator<WebElement> thenComparing(
			Function<? super WebElement, ? extends U> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public <U> Comparator<WebElement> thenComparing(
			Function<? super WebElement, ? extends U> arg0,
			Comparator<? super U> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Comparator<WebElement> thenComparingDouble(
			ToDoubleFunction<? super WebElement> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Comparator<WebElement> thenComparingInt(
			ToIntFunction<? super WebElement> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Comparator<WebElement> thenComparingLong(
			ToLongFunction<? super WebElement> arg0) {
		// TODO Auto-generated method stub
		return null;
	}


}
