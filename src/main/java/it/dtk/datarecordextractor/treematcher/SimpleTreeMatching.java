package it.dtk.datarecordextractor.treematcher;


import it.dtk.datarecordextractor.model.TreeAlignment;
import it.dtk.listflattener.model.*;

public class SimpleTreeMatching implements TreeMatcher {

	private static final short TRACKBACK_DIAGONAL = 0;
	private static final short TRACKBACK_UP = 1;
	private static final short TRACKBACK_LEFT = 2;


	/**
	 *
     * Given 2 WebElements, returns a TreeAlignment having as FirstNode the WebElement A and as SecondNode the WebElement B, if A and B match 
     * and as subchilds all matching children, otherwise return a TreeAlignment with FirstNode and SecondNode nulls 
	 * @param A: albero seme
	 * @param B: albero corrente.
	 */
	public static TreeAlignment align(WebElement A, WebElement B) {
		TreeAlignment returnAlignment;
		// se i due tagnode dei rispettivi alberi sono diversi allora score= 0.0 and tagelement(label) null;
		if (!(A.getNodeTag().equals(B.getNodeTag()))) {
			returnAlignment = new TreeAlignment();
			returnAlignment.setScore(0.00);
			return returnAlignment;
		}
		
		else { 
			returnAlignment = new TreeAlignment(A, B);
			double[][] matchMatrix = new double[A.getChildren().size() + 1][B.getChildren().size() + 1];
			// alignmentMatrix contains the treeAlignments among partial tree
			TreeAlignment[][] alignmentMatrix = new TreeAlignment[A.getChildren().size()][B.getChildren().size()];
			// matrice che conterr� il back trace
			short[][] trackbackMatrix = new short[A.getChildren().size()][B
					.getChildren().size()];

			for (int i = 1; i < matchMatrix.length; i++) {
				for (int j = 1; j < matchMatrix[i].length; j++) {
					if (matchMatrix[i][j - 1] > matchMatrix[i - 1][j]) {
						matchMatrix[i][j] = matchMatrix[i][j - 1];
						trackbackMatrix[i - 1][j - 1] = TRACKBACK_LEFT;
					} else {
						matchMatrix[i][j] = matchMatrix[i - 1][j];
						trackbackMatrix[i - 1][j - 1] = TRACKBACK_UP;
					}

					alignmentMatrix[i - 1][j - 1] = align(
							A.getChildren().get(i - 1),
							B.getChildren().get(j - 1));
					double diagonalScore = matchMatrix[i - 1][j - 1]
							+ alignmentMatrix[i - 1][j - 1].getScore();

					if (diagonalScore > matchMatrix[i][j]) {
						matchMatrix[i][j] = diagonalScore;
						trackbackMatrix[i - 1][j - 1] = TRACKBACK_DIAGONAL;
					}
				}
			}

			returnAlignment= trackBack(returnAlignment, alignmentMatrix, trackbackMatrix);
			returnAlignment.setScore(1.00 + matchMatrix[matchMatrix.length - 1][matchMatrix[0].length - 1]);
			return returnAlignment;
		}
	}

	/**
	 * Using trackBackMatrix adds to treeToAlin allined children
	 * @param treeToAlin
	 * @param alignmentMatrix
	 * @param trackbackMatrix
	 * @return treeToAlin
	 */
	private static TreeAlignment trackBack(TreeAlignment treeToAlin,
                                           TreeAlignment[][] alignmentMatrix, short[][] trackbackMatrix) {
		// lakukan trackback
		int trackbackRow = trackbackMatrix.length - 1;
		int trackbackColumn = -1;

		if (trackbackRow >= 0) {
			trackbackColumn = trackbackMatrix[0].length - 1;
		}

		while (trackbackRow >= 0 && trackbackColumn >= 0) {
			// jika ada node yang match
			if (trackbackMatrix[trackbackRow][trackbackColumn] == TRACKBACK_DIAGONAL) {
				treeToAlin.add(alignmentMatrix[trackbackRow][trackbackColumn]);
				trackbackRow--;
				trackbackColumn--;
			} else if (trackbackMatrix[trackbackRow][trackbackColumn] == TRACKBACK_UP) {
				trackbackRow--;
			} else if (trackbackMatrix[trackbackRow][trackbackColumn] == TRACKBACK_LEFT) {
				trackbackColumn--;
			}
		}

		return treeToAlin;

	}


}