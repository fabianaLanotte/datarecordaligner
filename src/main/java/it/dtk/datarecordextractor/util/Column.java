package it.dtk.datarecordextractor.util;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class Column implements Serializable{
	public int col;
	public List<Row> values;
	//public Row[] values;
	
	
	@JsonCreator
	public Column(@JsonProperty("col") int n,@JsonProperty("values") List<Row> v){
		this.col=n;
		this.values=v;
	}	
}

