package it.dtk.datarecordextractor.util;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ListJson implements Serializable {
	public int list;
	public List<Column> values;
	
	@JsonCreator
	public ListJson(@JsonProperty("list")int n, @JsonProperty("values")List<Column> value){
		this.list = n;
		this.values = value;
	}
}

