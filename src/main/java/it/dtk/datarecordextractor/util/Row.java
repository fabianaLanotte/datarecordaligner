package it.dtk.datarecordextractor.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Row {
	public int row;
	public String value;
	
	@JsonCreator
	public Row(@JsonProperty("row") int r, @JsonProperty("value") String v){
		this.row=r;
		this.value = v;
	}
	
	public int getRow(){return row;}
	
	public String getValue() {
		return value;
	}
	
}