package it.dtk.datarecordextractor.webServer;

import it.dtk.datarecordextractor.PartialTreeAligner;
import it.dtk.listflattener.ListExtractor;
import it.dtk.listflattener.model.WebList;
import it.dtk.listflattener.model.WebPage;
import it.dtk.traverser.TraverserFactory;
import it.dtk.utils.UnixEpoch;


public class ItemExtractor {
	
	public static String extract(String url, String urlTraverser){
		//String webSite = "http://"+url;
		double tagFactor = 0.4d;
		StringBuilder string = new StringBuilder();
		try {
			String urlService = urlTraverser;
			TraverserFactory.vpnUrl = urlService;
			WebPage webPage = ListExtractor.extract(url, tagFactor, UnixEpoch.getTimeStamp(), true);
			string.append("["); 
			int indexList=0;
			for (WebList l : webPage.getWebLists()) {
				PartialTreeAligner p = new PartialTreeAligner();
				p.alignDataRecords(l.getWebElements());
				string.append("{ \"list\" : "+indexList+",");
				string.append(" \"values\" : ");
				if(indexList == webPage.getWebLists().size()-1){
					string.append(p.stringJson()+"}");
				} else {
					string.append(p.stringJson()+"},");
				}
				indexList++;
			}
			string.append("]");
			System.out.println(string);
			//ListExtractor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return string.toString();
	}
}
