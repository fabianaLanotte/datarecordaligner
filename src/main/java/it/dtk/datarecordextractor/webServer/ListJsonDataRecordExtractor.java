package it.dtk.datarecordextractor.webServer;

import static spark.Spark.*;

/**
 * 
 * @author Colucci Pasquale
 *
 */

public class ListJsonDataRecordExtractor {

	
	public static void main(String[] args) {
		port(4569);
		/**
		 * Utilizzo del framework SparkJava, permette di effetuare una richiesta
		 * get. Restituisce i dataitem allineati e stampati in formato json.
		 * @Parma :url = http://localhost:4569/datarecordextractor/http://nomesito
		 */
		get("/datarecordextractor/*",(request, response) -> {
			response.type("application/json");
            String url = request.splat()[0];
            String urlService = "http://193.204.187.132:15000/traverseAsync?url=";
			String json = ItemExtractor.extract(url, urlService);
            //ObjectMapper mapper = new ObjectMapper();
			//return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
            return json;
		});
        get("/hello", (req, res) -> "Hello World ciao");


    }
}