package it.dtk.itemExtraction;

import java.util.ArrayList;
import java.util.List;

import ch.qos.logback.core.joran.action.NewRuleAction;
import it.dtk.datarecordextractor.PartialTreeAligner;
import it.dtk.datarecordextractor.util.ListJson;
import it.dtk.listflattener.ListExtractor;
import it.dtk.listflattener.model.WebList;
import it.dtk.listflattener.model.WebPage;
import it.dtk.traverser.TraverserFactory;
import it.dtk.utils.UnixEpoch;

public class TestReale {

	public static void main(String[] args) {
		String webSite = null;
		double tagFactor = 0.4d;
		//www.baritoday.it
		webSite = "http://farmaciapessolano.it/";
		try {
			double start = System.currentTimeMillis();
			String urlService = "http://193.204.187.132:15000/traverseAsync?url=";
			TraverserFactory.vpnUrl = urlService;
			WebPage webPage = ListExtractor.extract(webSite, tagFactor, UnixEpoch.getTimeStamp(), true);
			double time = (System.currentTimeMillis() - start) / 1000;
			System.out.println("Total time = " + time + " s");
			System.out.println("Lists found: " + webPage.getWebLists().size());
			StringBuilder string = new StringBuilder("[");
            int indexList=0;
			for (WebList l : webPage.getWebLists()) {
				PartialTreeAligner p = new PartialTreeAligner();
				p.alignDataRecords(l.getWebElements());
                string.append("{ \"list\" : "+indexList+",");
                string.append(" \"values\" : ");
                if(indexList == webPage.getWebLists().size()-1){
					string.append(p.stringJson()+"}");
				} else {
					string.append(p.stringJson()+"},");
				}
				//string.append(p.stringJson()+"},");
				//string.append(p.stringJson(indexList));
				
                indexList++;
			}
            string.append("]");
			System.out.println(string);
			ListExtractor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
