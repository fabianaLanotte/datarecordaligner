package it.dtk.junittest.partialtreealigner;

import static org.junit.Assert.*;
import it.dtk.datarecordextractor.PartialTreeAligner;
import it.dtk.datarecordextractor.model.TreeAlignment;
import it.dtk.datarecordextractor.treematcher.SimpleTreeMatching;
import it.dtk.listflattener.ListExtractor;
import it.dtk.listflattener.model.Orientation;
import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebList;
import it.dtk.listflattener.model.WebPage;
import it.dtk.utils.UnixEpoch;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class PartialTreeAlignerTest {

	@Test(timeout=50)
	public void testAlignDataRecordsPerformance() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
		List<WebElement> list = new ArrayList<WebElement>();

		WebElement root1 = new WebElement();
		root1.setNodeTag("A");
		root1.setId(0);
		root1.setWebPage(page);
		root1.setNumNodes(8);

		WebElement bTs = new WebElement();
		bTs.setNodeTag("B");
		bTs.setId(1);
		bTs.setWebPage(page);
		bTs.setText("<a href: link>");

		WebElement cTs = new WebElement();
		cTs.setNodeTag("C");
		cTs.setId(2);
		cTs.setWebPage(page);

		WebElement dTs = new WebElement();
		dTs.setNodeTag("D");
		dTs.setId(3);
		dTs.setWebPage(page);

		WebElement eTs = new WebElement();
		eTs.setNodeTag("E");
		eTs.setId(4);
		eTs.setWebPage(page);

		WebElement fTs = new WebElement();
		fTs.setNodeTag("F");
		fTs.setId(5);
		fTs.setWebPage(page);

		WebElement gTs = new WebElement();
		gTs.setNodeTag("G");
		gTs.setId(6);
		gTs.setWebPage(page);

		WebElement hTs = new WebElement();
		hTs.setNodeTag("H");
		hTs.setId(7);
		hTs.setWebPage(page);
		hTs.setText("libero");

		List<WebElement> childrenATs = new ArrayList<WebElement>();
		childrenATs.add(cTs);
		childrenATs.add(dTs);
		childrenATs.add(eTs);
		childrenATs.add(bTs);
		root1.setChildren(childrenATs);

		List<WebElement> childrenBTs = new ArrayList<WebElement>();
		childrenBTs.add(fTs);
		childrenBTs.add(gTs);
		childrenBTs.add(hTs);
		bTs.setChildren(childrenBTs);		
		//-------------------------------- Albero T1	
		WebElement root2 = new WebElement();
		root2.setNodeTag("A");
		root2.setId(8);
		root2.setWebPage(page);
		root2.setNumNodes(7);
		root2.setText("colore");

		WebElement xT1 = new WebElement();
		xT1.setNodeTag("X");
		xT1.setId(9);
		xT1.setWebPage(page);
		xT1.setText("ciao");

		WebElement bT1 = new WebElement();
		bT1.setNodeTag("B");
		bT1.setId(10);
		bT1.setWebPage(page);

		WebElement eT1 = new WebElement();
		eT1.setNodeTag("E");
		eT1.setId(11);
		eT1.setWebPage(page);

		WebElement gT1 = new WebElement();
		gT1.setNodeTag("G");
		gT1.setId(12);
		gT1.setWebPage(page);

		WebElement lT1 = new WebElement();
		lT1.setNodeTag("L");
		lT1.setId(13);
		lT1.setWebPage(page);

		WebElement hT1 = new WebElement();
		hT1.setNodeTag("H");
		hT1.setId(14);
		hT1.setWebPage(page);

		List<WebElement> childrenAT1 = new ArrayList<WebElement>();
		childrenAT1.add(eT1);
		childrenAT1.add(xT1);
		childrenAT1.add(bT1);
		root2.setChildren(childrenAT1);

		List<WebElement> childrenBT1 = new ArrayList<WebElement>();
		childrenBT1.add(gT1);
		childrenBT1.add(lT1);
		childrenBT1.add(hT1);
		bT1.setChildren(childrenBT1);
		//--------------------------------------------------T2
		WebElement root3 = new WebElement();
		root3.setNodeTag("A");
		root3.setId(15);
		root3.setWebPage(page);
		root3.setText("mail");

		WebElement cT2 = new WebElement();
		cT2.setNodeTag("C");
		cT2.setId(16);
		cT2.setWebPage(page);

		WebElement zT2 = new WebElement();
		zT2.setNodeTag("Z");
		zT2.setId(17);
		zT2.setWebPage(page);
		zT2.setText("flauto");
		//
		WebElement yT2 = new WebElement();
		yT2.setNodeTag("Y");
		yT2.setId(18);
		yT2.setWebPage(page);
		yT2.setText("alice");

		List<WebElement> childrenZT2 = new ArrayList<WebElement>();
		childrenZT2.add(yT2);
		zT2.setChildren(childrenZT2);

		WebElement eT2 = new WebElement();
		eT2.setNodeTag("E");
		eT2.setId(19);
		eT2.setWebPage(page);

		WebElement xT2 = new WebElement();
		xT2.setNodeTag("X");
		xT2.setId(20);
		xT2.setWebPage(page);

		WebElement bT2 = new WebElement();
		bT2.setNodeTag("B");
		bT2.setId(21);
		bT2.setWebPage(page);

		WebElement lT2 = new WebElement();
		lT2.setNodeTag("L");
		lT2.setId(22);
		lT2.setWebPage(page);

		List<WebElement> childrenAT2 = new ArrayList<WebElement>();
		childrenAT2.add(cT2);
		childrenAT2.add(zT2);
		childrenAT2.add(eT2);
		childrenAT2.add(xT2);
		childrenAT2.add(bT2);
		childrenAT2.add(lT2);
		root3.setChildren(childrenAT2);		
		//------------------------------------------------------T4		
		WebElement root4 = new WebElement();
		root4.setNodeTag("A");
		root4.setId(23);
		root4.setWebPage(page);
		root4.setText("sport");

		WebElement cT4 = new WebElement();
		cT4.setNodeTag("C");
		cT4.setId(24);
		cT4.setWebPage(page);

		WebElement zT4 = new WebElement();
		zT4.setNodeTag("Z");
		zT4.setId(25);
		zT4.setWebPage(page);
		zT4.setText("vento");

		WebElement yT4 = new WebElement();
		yT4.setNodeTag("Y");
		yT4.setId(26);
		yT4.setWebPage(page);
		yT4.setText("google");

		List<WebElement> childrenZT4 = new ArrayList<WebElement>();
		childrenZT4.add(yT4);
		zT4.setChildren(childrenZT4);

		List<WebElement> childrenAT4 = new ArrayList<WebElement>();
		childrenAT4.add(zT4);
		childrenAT4.add(cT4);
		root4.setChildren(childrenAT4);

		list.add(root1);
		list.add(root2);
		list.add(root3);
		list.add(root4);

		//SimpleTreeMatching matching = new SimpleTreeMatching();
		TreeAlignment align = SimpleTreeMatching.align(root1, root2);
		PartialTreeAligner p = new PartialTreeAligner();
		p.alignDataRecords(list);
	}

	@Test(expected=Exception.class)
	public void testAlignDataRecordsException() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
		List<WebElement> list = new ArrayList<WebElement>();
		PartialTreeAligner p = new PartialTreeAligner();
		p.alignDataRecords(list);
	}

	@Test(timeout=150)
	public void testStringJsonSingleList() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
		List<WebElement> list = new ArrayList<WebElement>();

		WebElement root1 = new WebElement();
		root1.setNodeTag("A");
		root1.setId(0);
		root1.setWebPage(page);
		root1.setNumNodes(8);
		root1.setText("");

		WebElement bTs = new WebElement();
		bTs.setNodeTag("B");
		bTs.setId(1);
		bTs.setWebPage(page);
		bTs.setText("<a href: link>");

		WebElement cTs = new WebElement();
		cTs.setNodeTag("C");
		cTs.setId(2);
		cTs.setWebPage(page);
		cTs.setText("");

		WebElement dTs = new WebElement();
		dTs.setNodeTag("D");
		dTs.setId(3);
		dTs.setWebPage(page);
		dTs.setText("");

		WebElement eTs = new WebElement();
		eTs.setNodeTag("E");
		eTs.setId(4);
		eTs.setWebPage(page);
		eTs.setText("");

		WebElement fTs = new WebElement();
		fTs.setNodeTag("F");
		fTs.setId(5);
		fTs.setWebPage(page);
		fTs.setText("");

		WebElement gTs = new WebElement();
		gTs.setNodeTag("G");
		gTs.setId(6);
		gTs.setWebPage(page);
		gTs.setText("");

		WebElement hTs = new WebElement();
		hTs.setNodeTag("H");
		hTs.setId(7);
		hTs.setWebPage(page);
		hTs.setText("libero");

		List<WebElement> childrenATs = new ArrayList<WebElement>();
		childrenATs.add(cTs);
		childrenATs.add(dTs);
		childrenATs.add(eTs);
		childrenATs.add(bTs);
		root1.setChildren(childrenATs);
		
		List<WebElement> childrenBTs = new ArrayList<WebElement>();
		childrenBTs.add(fTs);
		childrenBTs.add(gTs);
		childrenBTs.add(hTs);
		bTs.setChildren(childrenBTs);		
		//-------------------------------- Albero T1	
		WebElement root2 = new WebElement();
		root2.setNodeTag("A");
		root2.setId(8);
		root2.setWebPage(page);
		root2.setNumNodes(7);
		root2.setText("colore");

		WebElement xT1 = new WebElement();
		xT1.setNodeTag("X");
		xT1.setId(9);
		xT1.setWebPage(page);
		xT1.setText("ciao");

		WebElement bT1 = new WebElement();
		bT1.setNodeTag("B");
		bT1.setId(10);
		bT1.setWebPage(page);
		bT1.setText("");

		WebElement eT1 = new WebElement();
		eT1.setNodeTag("E");
		eT1.setId(11);
		eT1.setWebPage(page);
		eT1.setText("");

		WebElement gT1 = new WebElement();
		gT1.setNodeTag("G");
		gT1.setId(12);
		gT1.setWebPage(page);
		gT1.setText("");

		WebElement lT1 = new WebElement();
		lT1.setNodeTag("L");
		lT1.setId(13);
		lT1.setWebPage(page);
		lT1.setText("");

		WebElement hT1 = new WebElement();
		hT1.setNodeTag("H");
		hT1.setId(14);
		hT1.setWebPage(page);
		hT1.setText("");

		List<WebElement> childrenAT1 = new ArrayList<WebElement>();
		childrenAT1.add(eT1);
		childrenAT1.add(xT1);
		childrenAT1.add(bT1);
		root2.setChildren(childrenAT1);

		List<WebElement> childrenBT1 = new ArrayList<WebElement>();
		childrenBT1.add(gT1);
		childrenBT1.add(lT1);
		childrenBT1.add(hT1);
		bT1.setChildren(childrenBT1);
		//--------------------------------------------------T2
		WebElement root3 = new WebElement();
		root3.setNodeTag("A");
		root3.setId(15);
		root3.setWebPage(page);
		root3.setText("mail");

		WebElement cT2 = new WebElement();
		cT2.setNodeTag("C");
		cT2.setId(16);
		cT2.setWebPage(page);
		cT2.setText("");

		WebElement zT2 = new WebElement();
		zT2.setNodeTag("Z");
		zT2.setId(17);
		zT2.setWebPage(page);
		zT2.setText("flauto");
		//
		WebElement yT2 = new WebElement();
		yT2.setNodeTag("Y");
		yT2.setId(18);
		yT2.setWebPage(page);
		yT2.setText("alice");

		List<WebElement> childrenZT2 = new ArrayList<WebElement>();
		childrenZT2.add(yT2);
		zT2.setChildren(childrenZT2);
		//

		WebElement eT2 = new WebElement();
		eT2.setNodeTag("E");
		eT2.setId(19);
		eT2.setWebPage(page);
		eT2.setText("");

		WebElement xT2 = new WebElement();
		xT2.setNodeTag("X");
		xT2.setId(20);
		xT2.setWebPage(page);
		xT2.setText("");

		WebElement bT2 = new WebElement();
		bT2.setNodeTag("B");
		bT2.setId(21);
		bT2.setWebPage(page);
		bT2.setText("");

		WebElement lT2 = new WebElement();
		lT2.setNodeTag("L");
		lT2.setId(22);
		lT2.setWebPage(page);
		lT2.setText("");

		List<WebElement> childrenAT2 = new ArrayList<WebElement>();
		childrenAT2.add(cT2);
		childrenAT2.add(zT2);
		childrenAT2.add(eT2);
		childrenAT2.add(xT2);
		childrenAT2.add(bT2);
		childrenAT2.add(lT2);
		root3.setChildren(childrenAT2);		
		//------------------------------------------------------T4		
		WebElement root4 = new WebElement();
		root4.setNodeTag("A");
		root4.setId(23);
		root4.setWebPage(page);
		root4.setText("sport");

		WebElement cT4 = new WebElement();
		cT4.setNodeTag("C");
		cT4.setId(24);
		cT4.setWebPage(page);
		cT4.setText("");

		WebElement zT4 = new WebElement();
		zT4.setNodeTag("Z");
		zT4.setId(25);
		zT4.setWebPage(page);
		zT4.setText("vento");

		WebElement yT4 = new WebElement();
		yT4.setNodeTag("Y");
		yT4.setId(26);
		yT4.setWebPage(page);
		yT4.setText("google");

		List<WebElement> childrenZT4 = new ArrayList<WebElement>();
		childrenZT4.add(yT4);
		zT4.setChildren(childrenZT4);
		

		List<WebElement> childrenAT4 = new ArrayList<WebElement>();
		childrenAT4.add(zT4);
		childrenAT4.add(cT4);
		root4.setChildren(childrenAT4);

		list.add(root1);
		list.add(root2);
		list.add(root3);
		list.add(root4);

		SimpleTreeMatching matching = new SimpleTreeMatching();
		PartialTreeAligner p = new PartialTreeAligner();
		p.alignDataRecords(list);
		String json = p.stringJson();
	}

	@Test
	public void testStringJsonReturnNotNull() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
		List<WebElement> list = new ArrayList<WebElement>();

		WebElement root1 = new WebElement();
		root1.setNodeTag("A");
		root1.setId(0);
		root1.setWebPage(page);
		root1.setNumNodes(8);	
		//------------------------------------------------------T4		
		WebElement root4 = new WebElement();
		root4.setNodeTag("A");
		root4.setId(23);
		root4.setWebPage(page);
		root4.setText("sport");

		list.add(root1);
		list.add(root4);

		PartialTreeAligner p = new PartialTreeAligner();
		p.alignDataRecords(list);
		String json = p.stringJson();
		Assert.assertTrue(json != null);
	}

	@Test
	public void testStringJsonOK() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
		List<WebElement> list = new ArrayList<WebElement>();

		WebElement root1 = new WebElement();
		root1.setNodeTag("A");
		root1.setId(0);
		root1.setWebPage(page);
		root1.setNumNodes(8);

		WebElement bTs = new WebElement();
		bTs.setNodeTag("B");
		bTs.setId(1);
		bTs.setWebPage(page);

		WebElement cTs = new WebElement();
		cTs.setNodeTag("C");
		cTs.setId(2);
		cTs.setWebPage(page);

		WebElement dTs = new WebElement();
		dTs.setNodeTag("D");
		dTs.setId(3);
		dTs.setWebPage(page);

		WebElement eTs = new WebElement();
		eTs.setNodeTag("E");
		eTs.setId(4);
		eTs.setWebPage(page);

		WebElement fTs = new WebElement();
		fTs.setNodeTag("F");
		fTs.setId(5);
		fTs.setWebPage(page);

		WebElement gTs = new WebElement();
		gTs.setNodeTag("G");
		gTs.setId(6);
		gTs.setWebPage(page);

		WebElement hTs = new WebElement();
		hTs.setNodeTag("H");
		hTs.setId(7);
		hTs.setWebPage(page);


		List<WebElement> childrenATs = new ArrayList<WebElement>();
		childrenATs.add(cTs);
		childrenATs.add(dTs);
		childrenATs.add(eTs);
		childrenATs.add(bTs);
		root1.setChildren(childrenATs);

		List<WebElement> childrenBTs = new ArrayList<WebElement>();
		childrenBTs.add(fTs);
		childrenBTs.add(gTs);
		childrenBTs.add(hTs);
		bTs.setChildren(childrenBTs);		
		//-------------------------------- Albero T1	
		WebElement root2 = new WebElement();
		root2.setNodeTag("A");
		root2.setId(8);
		root2.setWebPage(page);
		root2.setNumNodes(7);


		WebElement xT1 = new WebElement();
		xT1.setNodeTag("X");
		xT1.setId(9);
		xT1.setWebPage(page);
		xT1.setText("ciao");

		WebElement bT1 = new WebElement();
		bT1.setNodeTag("B");
		bT1.setId(10);
		bT1.setWebPage(page);

		WebElement eT1 = new WebElement();
		eT1.setNodeTag("E");
		eT1.setId(11);
		eT1.setWebPage(page);

		WebElement gT1 = new WebElement();
		gT1.setNodeTag("G");
		gT1.setId(12);
		gT1.setWebPage(page);

		WebElement lT1 = new WebElement();
		lT1.setNodeTag("L");
		lT1.setId(13);
		lT1.setWebPage(page);

		WebElement hT1 = new WebElement();
		hT1.setNodeTag("H");
		hT1.setId(14);
		hT1.setWebPage(page);

		List<WebElement> childrenAT1 = new ArrayList<WebElement>();
		childrenAT1.add(eT1);
		childrenAT1.add(xT1);
		childrenAT1.add(bT1);
		root2.setChildren(childrenAT1);

		List<WebElement> childrenBT1 = new ArrayList<WebElement>();
		childrenBT1.add(gT1);
		childrenBT1.add(lT1);
		childrenBT1.add(hT1);
		bT1.setChildren(childrenBT1);
		//--------------------------------------------------T2
		WebElement root3 = new WebElement();
		root3.setNodeTag("A");
		root3.setId(15);
		root3.setWebPage(page);

		WebElement cT2 = new WebElement();
		cT2.setNodeTag("C");
		cT2.setId(16);
		cT2.setWebPage(page);

		WebElement zT2 = new WebElement();
		zT2.setNodeTag("Z");
		zT2.setId(17);
		zT2.setWebPage(page);

		WebElement yT2 = new WebElement();
		yT2.setNodeTag("Y");
		yT2.setId(18);
		yT2.setWebPage(page);


		List<WebElement> childrenZT2 = new ArrayList<WebElement>();
		childrenZT2.add(yT2);
		zT2.setChildren(childrenZT2);

		WebElement eT2 = new WebElement();
		eT2.setNodeTag("E");
		eT2.setId(19);
		eT2.setWebPage(page);

		WebElement xT2 = new WebElement();
		xT2.setNodeTag("X");
		xT2.setId(20);
		xT2.setWebPage(page);

		WebElement bT2 = new WebElement();
		bT2.setNodeTag("B");
		bT2.setId(21);
		bT2.setWebPage(page);

		WebElement lT2 = new WebElement();
		lT2.setNodeTag("L");
		lT2.setId(22);
		lT2.setWebPage(page);

		List<WebElement> childrenAT2 = new ArrayList<WebElement>();
		childrenAT2.add(cT2);
		childrenAT2.add(zT2);
		childrenAT2.add(eT2);
		childrenAT2.add(xT2);
		childrenAT2.add(bT2);
		childrenAT2.add(lT2);
		root3.setChildren(childrenAT2);		
		//------------------------------------------------------T4		
		WebElement root4 = new WebElement();
		root4.setNodeTag("A");
		root4.setId(23);
		root4.setWebPage(page);

		WebElement cT4 = new WebElement();
		cT4.setNodeTag("C");
		cT4.setId(24);
		cT4.setWebPage(page);

		WebElement zT4 = new WebElement();
		zT4.setNodeTag("Z");
		zT4.setId(25);
		zT4.setWebPage(page);

		WebElement yT4 = new WebElement();
		yT4.setNodeTag("Y");
		yT4.setId(26);
		yT4.setWebPage(page);

		List<WebElement> childrenZT4 = new ArrayList<WebElement>();
		childrenZT4.add(yT4);
		zT4.setChildren(childrenZT4);

		List<WebElement> childrenAT4 = new ArrayList<WebElement>();
		childrenAT4.add(zT4);
		childrenAT4.add(cT4);
		root4.setChildren(childrenAT4);
		
		list.add(root1);
		list.add(root2);
		list.add(root3);
		list.add(root4);
		
		SimpleTreeMatching matching = new SimpleTreeMatching();
		TreeAlignment align = matching.align(root1, root2);
		PartialTreeAligner p = new PartialTreeAligner();
		p.alignDataRecords(list);
	}

	@Test(timeout=3500)
	public void testStringjsonPerformanceMoreListReal() {
		String webSite = null;
		double tagFactor = 0.4d;
		webSite = "http://www.uniba.it";
		try {
			double start = System.currentTimeMillis();
			WebPage webPage = ListExtractor.extract(webSite, tagFactor, UnixEpoch.getTimeStamp(), true);
			double time = (System.currentTimeMillis() - start) / 1000;
			System.out.println("Total time = " + time + " s");
			System.out.println("Lists found: " + webPage.getWebLists().size());
			StringBuilder string = new StringBuilder();
			for (WebList l : webPage.getWebLists()) {
				PartialTreeAligner p = new PartialTreeAligner();
				p.alignDataRecords(l.getWebElements());
				string.append(p.stringJson());
			}
			ListExtractor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}