package it.dtk.junittest.simpletreematching;

import static org.junit.Assert.*;
import it.dtk.datarecordextractor.model.TreeAlignment;
import it.dtk.datarecordextractor.treematcher.SimpleTreeMatching;
import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebPage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class SimpleTreeMatchingJunitTest {


	
	@Test
	public void testAlignFailures() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);

		WebElement root1 = new WebElement();
		root1.setNodeTag("A");
		root1.setId(0);
		root1.setWebPage(page);
		root1.setNumNodes(8);

		WebElement bTs = new WebElement();
		bTs.setNodeTag("B");
		bTs.setId(4);
		bTs.setWebPage(page);

		WebElement cTs = new WebElement();
		cTs.setNodeTag("C");
		cTs.setId(1);
		cTs.setWebPage(page);

		WebElement dTs = new WebElement();
		dTs.setNodeTag("D");
		dTs.setId(2);
		dTs.setWebPage(page);

		WebElement eTs = new WebElement();
		eTs.setNodeTag("E");
		eTs.setId(3);
		eTs.setWebPage(page);

		WebElement fTs = new WebElement();
		fTs.setNodeTag("F");
		fTs.setId(5);
		fTs.setWebPage(page);

		WebElement gTs = new WebElement();
		gTs.setNodeTag("G");
		gTs.setId(6);
		gTs.setWebPage(page);

		WebElement hTs = new WebElement();
		hTs.setNodeTag("H");
		hTs.setId(7);
		hTs.setWebPage(page);

		List<WebElement> childrenATs = new ArrayList<WebElement>();
		childrenATs.add(cTs);
		childrenATs.add(dTs);
		childrenATs.add(eTs);
		childrenATs.add(bTs);
		root1.setChildren(childrenATs);

		List<WebElement> childrenBTs = new ArrayList<WebElement>();
		childrenBTs.add(fTs);
		childrenBTs.add(gTs);
		childrenBTs.add(hTs);
		bTs.setChildren(childrenBTs);
		//-------------------------------- Albero T1
		WebElement root2 = new WebElement();
		root2.setNodeTag("A");
		root2.setId(0);
		root2.setWebPage(page);
		root2.setNumNodes(7);

		WebElement xTi = new WebElement();
		xTi.setNodeTag("X");
		xTi.setId(2);
		xTi.setWebPage(page);

		WebElement bTi = new WebElement();
		bTi.setNodeTag("B");
		bTi.setId(3);
		bTi.setWebPage(page);

		WebElement eTi = new WebElement();
		eTi.setNodeTag("E");
		eTi.setId(1);
		eTi.setWebPage(page);

		WebElement gTi = new WebElement();
		gTi.setNodeTag("G");
		gTi.setId(4);
		gTi.setWebPage(page);

		WebElement lTi = new WebElement();
		lTi.setNodeTag("L");
		lTi.setId(5);
		lTi.setWebPage(page);

		WebElement hTi = new WebElement();
		hTi.setNodeTag("H");
		hTi.setId(6);
		hTi.setWebPage(page);

		List<WebElement> childrenATi = new ArrayList<WebElement>();
		childrenATi.add(eTi);
		childrenATi.add(xTi);
		childrenATi.add(bTi);
		root2.setChildren(childrenATi);

		List<WebElement> childrenBTi = new ArrayList<WebElement>();
		childrenBTi.add(gTi);
		childrenBTi.add(lTi);
		childrenBTi.add(hTi);
		bTi.setChildren(childrenBTi);
		
		SimpleTreeMatching matching = new SimpleTreeMatching();
		TreeAlignment align = matching.align(root1, root2);
		double score = align.getScore();
		double exactScore=4.0;

		assertNotEquals(exactScore, score);
	}
	
	@Test
	public void testAlignOK() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
	
		WebElement root1 = new WebElement();
		root1.setNodeTag("A");
		root1.setId(0);
		root1.setWebPage(page);
		root1.setNumNodes(8);

		WebElement bTs = new WebElement();
		bTs.setNodeTag("B");
		bTs.setId(4);
		bTs.setWebPage(page);

		WebElement cTs = new WebElement();
		cTs.setNodeTag("C");
		cTs.setId(1);
		cTs.setWebPage(page);

		WebElement dTs = new WebElement();
		dTs.setNodeTag("D");
		dTs.setId(2);
		dTs.setWebPage(page);

		WebElement eTs = new WebElement();
		eTs.setNodeTag("E");
		eTs.setId(3);
		eTs.setWebPage(page);

		WebElement fTs = new WebElement();
		fTs.setNodeTag("F");
		fTs.setId(5);
		fTs.setWebPage(page);

		WebElement gTs = new WebElement();
		gTs.setNodeTag("G");
		gTs.setId(6);
		gTs.setWebPage(page);

		WebElement hTs = new WebElement();
		hTs.setNodeTag("H");
		hTs.setId(7);
		hTs.setWebPage(page);

		List<WebElement> childrenATs = new ArrayList<WebElement>();
		childrenATs.add(cTs);
		childrenATs.add(dTs);
		childrenATs.add(eTs);
		childrenATs.add(bTs);
		root1.setChildren(childrenATs);

		List<WebElement> childrenBTs = new ArrayList<WebElement>();
		childrenBTs.add(fTs);
		childrenBTs.add(gTs);
		childrenBTs.add(hTs);
		bTs.setChildren(childrenBTs);
		//-------------------------------- Albero T1
		WebElement root2 = new WebElement();
		root2.setNodeTag("A");
		root2.setId(0);
		root2.setWebPage(page);
		root2.setNumNodes(7);

		WebElement xTi = new WebElement();
		xTi.setNodeTag("X");
		xTi.setId(2);
		xTi.setWebPage(page);

		WebElement bTi = new WebElement();
		bTi.setNodeTag("B");
		bTi.setId(3);
		bTi.setWebPage(page);

		WebElement eTi = new WebElement();
		eTi.setNodeTag("E");
		eTi.setId(1);
		eTi.setWebPage(page);

		WebElement gTi = new WebElement();
		gTi.setNodeTag("G");
		gTi.setId(4);
		gTi.setWebPage(page);

		WebElement lTi = new WebElement();
		lTi.setNodeTag("L");
		lTi.setId(5);
		lTi.setWebPage(page);


		WebElement hTi = new WebElement();
		hTi.setNodeTag("H");
		hTi.setId(6);
		hTi.setWebPage(page);

		List<WebElement> childrenATi = new ArrayList<WebElement>();
		childrenATi.add(eTi);
		childrenATi.add(xTi);
		childrenATi.add(bTi);
		root2.setChildren(childrenATi);

		List<WebElement> childrenBTi = new ArrayList<WebElement>();
		childrenBTi.add(gTi);
		childrenBTi.add(lTi);
		childrenBTi.add(hTi);
		bTi.setChildren(childrenBTi);
		
		SimpleTreeMatching matching = new SimpleTreeMatching();
		TreeAlignment align = matching.align(root1, root2);
		double score = align.getScore();
		double exactScore=5.0;

		assertEquals(exactScore, score, 0);
	}
	
	@Test
	public void testAlignReturnTypeOK() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
	
		WebElement root1 = new WebElement();
		root1.setNodeTag("A");
		root1.setId(0);
		root1.setWebPage(page);
		root1.setNumNodes(8);		
		//-------------------------------- Albero T1
		WebElement root2 = new WebElement();
		root2.setNodeTag("A");
		root2.setId(0);
		root2.setWebPage(page);
		root2.setNumNodes(7);
		
		SimpleTreeMatching matching = new SimpleTreeMatching();
		TreeAlignment align = matching.align(root1, root2);
		Assert.assertSame(align.getClass(), TreeAlignment.class);
	}
	
	@Test
	public void testAlignNotNull() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
	
		WebElement root1 = new WebElement();
		root1.setNodeTag("A");
		root1.setId(0);
		root1.setWebPage(page);
		root1.setNumNodes(8);		
		//-------------------------------- Albero T1
		WebElement root2 = new WebElement();
		root2.setNodeTag("A");
		root2.setId(0);
		root2.setWebPage(page);
		root2.setNumNodes(7);
		
		SimpleTreeMatching matching = new SimpleTreeMatching();
		TreeAlignment align = matching.align(root1, root2);
		Assert.assertNotNull(align);

	}
	
	@Test
	public void testAlignUnMatching() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
	
		WebElement root1 = new WebElement();
		root1.setNodeTag("A");
		root1.setId(0);
		root1.setWebPage(page);
		root1.setNumNodes(1);		
		//-------------------------------- Albero T1
		WebElement root2 = new WebElement();
		root2.setNodeTag("B");
		root2.setId(1);
		root2.setWebPage(page);
		root2.setNumNodes(1);
		
		SimpleTreeMatching matching = new SimpleTreeMatching();
		TreeAlignment align = matching.align(root1, root2);
		double score = align.getScore();
		Assert.assertEquals(0, score, 0);

	}
	
	@Test(expected = Exception.class)
	public void testAlignInputNull() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
	
		WebElement root1 = null;
		//-------------------------------- Albero T1
		WebElement root2 = null;
		
		SimpleTreeMatching matching = new SimpleTreeMatching();
		TreeAlignment align = matching.align(root1, root2);
	}
	
	
	@Test(expected = Exception.class)
	public void testAlignOnlyOneInputNull() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
	
		WebElement root1 = null;
		//-------------------------------- Albero T1
		WebElement root2 = new WebElement();
		root2.setNodeTag("B");
		root2.setId(1);
		root2.setWebPage(page);
		root2.setNumNodes(1);

		SimpleTreeMatching matching = new SimpleTreeMatching();
		TreeAlignment align = matching.align(root1, root2);
	}
	
	@Test(timeout = 15)
	public void testAlignPerformance() {
		Date d = new Date();
		long t = d.getTime();
		WebPage page = new WebPage("www.baritoday.it",t);
	
		WebElement root1 = new WebElement();
		root1.setNodeTag("A");
		root1.setId(0);
		root1.setWebPage(page);
		root1.setNumNodes(8);

		WebElement bTs = new WebElement();
		bTs.setNodeTag("B");
		bTs.setId(4);
		bTs.setWebPage(page);

		WebElement cTs = new WebElement();
		cTs.setNodeTag("C");
		cTs.setId(1);
		cTs.setWebPage(page);

		WebElement dTs = new WebElement();
		dTs.setNodeTag("D");
		dTs.setId(2);
		dTs.setWebPage(page);

		WebElement eTs = new WebElement();
		eTs.setNodeTag("E");
		eTs.setId(3);
		eTs.setWebPage(page);

		WebElement fTs = new WebElement();
		fTs.setNodeTag("F");
		fTs.setId(5);
		fTs.setWebPage(page);

		WebElement gTs = new WebElement();
		gTs.setNodeTag("G");
		gTs.setId(6);
		gTs.setWebPage(page);

		WebElement hTs = new WebElement();
		hTs.setNodeTag("H");
		hTs.setId(7);
		hTs.setWebPage(page);

		List<WebElement> childrenATs = new ArrayList<WebElement>();
		childrenATs.add(cTs);
		childrenATs.add(dTs);
		childrenATs.add(eTs);
		childrenATs.add(bTs);
		root1.setChildren(childrenATs);

		List<WebElement> childrenBTs = new ArrayList<WebElement>();
		childrenBTs.add(fTs);
		childrenBTs.add(gTs);
		childrenBTs.add(hTs);
		bTs.setChildren(childrenBTs);
		//-------------------------------- Albero T1
		WebElement root2 = new WebElement();
		root2.setNodeTag("A");
		root2.setId(0);
		root2.setWebPage(page);
		root2.setNumNodes(7);

		WebElement xTi = new WebElement();
		xTi.setNodeTag("X");
		xTi.setId(2);
		xTi.setWebPage(page);

		WebElement bTi = new WebElement();
		bTi.setNodeTag("B");
		bTi.setId(3);
		bTi.setWebPage(page);

		WebElement eTi = new WebElement();
		eTi.setNodeTag("E");
		eTi.setId(1);
		eTi.setWebPage(page);

		WebElement gTi = new WebElement();
		gTi.setNodeTag("G");
		gTi.setId(4);
		gTi.setWebPage(page);

		WebElement lTi = new WebElement();
		lTi.setNodeTag("L");
		lTi.setId(5);
		lTi.setWebPage(page);


		WebElement hTi = new WebElement();
		hTi.setNodeTag("H");
		hTi.setId(6);
		hTi.setWebPage(page);

		List<WebElement> childrenATi = new ArrayList<WebElement>();
		childrenATi.add(eTi);
		childrenATi.add(xTi);
		childrenATi.add(bTi);
		root2.setChildren(childrenATi);

		List<WebElement> childrenBTi = new ArrayList<WebElement>();
		childrenBTi.add(gTi);
		childrenBTi.add(lTi);
		childrenBTi.add(hTi);
		bTi.setChildren(childrenBTi);
		
		SimpleTreeMatching matching = new SimpleTreeMatching();
		TreeAlignment align = matching.align(root1, root2);

	}
}
